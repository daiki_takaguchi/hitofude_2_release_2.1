﻿using UnityEngine;
using System.Collections;

public class window_result_controller : MonoBehaviour {

	// Use this for initialization

	public GameObject good;
	public GameObject fantastic;
	public GameObject perfect;

	public enum result{
		good,
		fantastic,
		perfect
	}

	void Start () {
		//good.SetActive (false);
		//fantastic.SetActive (false);
		//perfect.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void show_result(result input_result){

		print ("show_result:"+ input_result);

		if (input_result == result.good) {
			good.SetActive (true);
			fantastic.SetActive (false);
			perfect.SetActive (false);
		}else if(input_result == result.fantastic) {
			good.SetActive (false);
			fantastic.SetActive (true);
			perfect.SetActive (false);
		}else if(input_result == result.perfect){
			good.SetActive (false);
			fantastic.SetActive (false);
			perfect.SetActive (true);
		}

	}

	public void hide_result(){
		good.SetActive (false);
		fantastic.SetActive (false);
		perfect.SetActive (false);
	}
}
