﻿using UnityEngine;
using System.Collections;

public class answer_point_animation : MonoBehaviour {

	// Use this for initialization

	public UI_controller ui;
	public bool is_dan = false;
	public GameObject point_anim;
	public GameObject label;

	public Transform scale;
	public Transform position;


	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


	}

	public void set_position(Vector3 input_position){

		Vector3 temp = position.localPosition;

		position.localPosition = new Vector3 (input_position.x,input_position.y,temp.z);
	}

	public void animation_start(){
		point_anim.SetActive (true);
		label.SetActive (true);
		GetComponent<Animator> ().enabled = true;
		GetComponent<Animator> ().Play ("answer_point_anim");

	}

	public void animation_end(){
		point_anim.SetActive (false);
		label.SetActive (false);
	
		//GetComponent<Animator> ().Play ("normal");
		GetComponent<Animator> ().enabled = false;

	}
}
