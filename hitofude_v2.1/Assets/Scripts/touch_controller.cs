﻿using UnityEngine;
using System.Collections;

public class touch_controller : MonoBehaviour {

	// Use this for initialization

	public hitofude_data_controller hitofude_data;
	public Vector3 prev;

	public enum touch_event{
		Clicked,
		Mouse_Up

	}

	void Start () {
		//TouchPhase.

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.touchCount > 0){

			for(int i= 0; i<Input.touchCount; i++){


				if(Input.touches[i].phase==TouchPhase.Began){

					cast_ray(Input.touches[i].position,touch_event.Clicked);
				}
			}


			
		}

		if(Input.GetMouseButton(0)){
			cast_ray(Input.mousePosition,touch_event.Clicked);
		}

		if(Input.GetMouseButtonUp(0)){
			cast_ray(Input.mousePosition,touch_event.Mouse_Up);
		}
	
	}

	void cast_ray(Vector3 point, touch_event input_touch ){
		Ray ray= Camera.main.ScreenPointToRay(point);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit)){
			if(input_touch==touch_event.Clicked){
				click(hit.collider.gameObject);
			}else if(input_touch==touch_event.Mouse_Up){
				mouse_up(hit.collider.gameObject);

				print ("hit");
			}
		}
	}

	void click(GameObject hit){

		if(hit.GetComponent<hitofude_point_controller>()!=null){
			hitofude_data.point_clicked(hit);
		}
		//hitofude_line_controller.

	}

	void mouse_up(GameObject hit){

		if(hit.GetComponent<hitofude_point_controller>()!=null){
			hitofude_data.point_clicked(hit);
		}
	}

	void test(Vector3 point){

		hitofude_draw_controller.draw_line(GameObject.Find ("line"),prev,point);
		prev = point;
	}



	


}
