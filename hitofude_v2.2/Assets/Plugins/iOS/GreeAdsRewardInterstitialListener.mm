//
//  GreeAdsRewardInterstitialListener.m
//  GreeAdsRewardInterstitialListener
//  VERSION: 1.1.0
//
//  Copyright 2013 GREE, inc. All rights reserved.
//
//
#import "GreeAdsRewardInterstitialListener.h"

// void UnitySendMessage(const char* obj, const char* method, const char* msg);

@implementation GreeAdsRewardInterstitialListener

//
// turn src to a cstring, then copy it into dst
//
-(BOOL)NSStringToCString:(NSString*)src dst:(char **)dst
{
    if( src == NULL ) return NO;
    if( (*dst = (char *)malloc([src length])) == NULL ) return NO;
    
    strcpy(*dst, [src UTF8String]);
    
    return YES;
}

-(id)initWithName:(NSString *)callbackObjectName
{
    if ( self = [super init] ) {
        if (callbackObjectName != NULL ) {
            // [NSString UTF8String] will free the memory automatically.
            // we have to copy the string into unityCallbackObjectName
            [self NSStringToCString:callbackObjectName dst:&unityCallbackObjectName];
        }
    }
    
    return self;
}

-(void)onGreeAdsRewardInterstitialStartLoading
{
    if (unityCallbackObjectName != NULL) {
        UnitySendMessage(unityCallbackObjectName, "onGreeAdsRewardInterstitialStartLoading", "");
    }
}

-(void)onGreeAdsRewardInterstitialViewWillAppear
{
    if (unityCallbackObjectName != NULL) {
        UnitySendMessage(unityCallbackObjectName, "onGreeAdsRewardInterstitialViewWillAppear", "");
    }
}

-(void)onGreeAdsRewardInterstitialViewDidAppear
{
    if (unityCallbackObjectName != NULL) {
        UnitySendMessage(unityCallbackObjectName, "onGreeAdsRewardInterstitialViewDidAppear", "");
    }
}

-(void)onGreeAdsRewardInterstitialViewWillClose
{
    if (unityCallbackObjectName != NULL) {
        UnitySendMessage(unityCallbackObjectName, "onGreeAdsRewardInterstitialViewWillClose", "");
    }
}

-(void)onGreeAdsRewardInterstitialViewDidClose
{
    if (unityCallbackObjectName != NULL) {
        UnitySendMessage(unityCallbackObjectName, "onGreeAdsRewardInterstitialViewDidClose", "");
    }
}

-(void)onGreeAdsRewardInterstitialLoadFailed
{
    if (unityCallbackObjectName != NULL) {
        UnitySendMessage(unityCallbackObjectName, "onGreeAdsRewardInterstitialLoadFailed", "");
    }
}

@end
