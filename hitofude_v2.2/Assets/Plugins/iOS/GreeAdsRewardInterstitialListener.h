//
//  GreeAdsRewardInterstitialListener.h
//  GreeAdsRewardInterstitialListener
//  VERSION: 1.0.0
//
//  Copyright 2013 GREE, inc. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import "GreeAdsRewardInterstitialProtocol.h"

@interface GreeAdsRewardInterstitialListener : NSObject <GreeAdsRewardInterstitialProtocol>
{
    char* unityCallbackObjectName;
}

-(id)initWithName:(NSString *)callbackObjectName;

@end
