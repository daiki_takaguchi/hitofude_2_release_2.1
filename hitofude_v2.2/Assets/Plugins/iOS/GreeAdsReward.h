//
//  GreeAdsReward.h
//  GreeAdsReward
//  VERSION: 2.3.0
//
//  Copyright 2013 GREE, inc. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum  {
	GreeAdsRewardCampaignTypeNone,
	GreeAdsRewardCampaignTypeNormal,
	GreeAdsRewardCampaignTypeXpromotion,
	GreeAdsRewardCampaignTypeAll,
}GreeAdsRewardCampaignType;

@interface GreeAdsReward : NSObject

//return sdk version
+ (NSString *)getVersion;

//set application info from reward
+ (void)setSiteID:(NSString *)siteID siteKey:(NSString *)siteKey;
+ (void)setSiteID:(NSString *)siteID siteKey:(NSString *)siteKey useSandBox:(BOOL)useSandbox;

+ (void)setDevMode:(BOOL)devMode;

//return offerwall View
+(UIView *)OfferwallViewWithFrame:(CGRect)frame mediaID:(NSString *)mediaID identifier:(NSString *)identifier;
//return offerwall View for specific campaign
+(UIView *)OfferwallViewWithFrame:(CGRect)frame mediaID:(NSString *)mediaID identifier:(NSString *)identifier campaignID:(NSString *)campaignID;

//show OfferwallView in full screen
+(void)showOfferWallWithMeidaID:(NSString*)mediaID identifier:(NSString*)identifier;
//show campaign's detail page
+(void)showOfferWallWithMeidaID:(NSString*)mediaID identifier:(NSString*)identifier campaignID:(NSString *)campaignID;

//popup a offerwall view, this is used for interstitial and cross promotion
+(void)showInterstitial:(NSString *)mediaID identifier:(NSString*)identifier campaignType:(GreeAdsRewardCampaignType)type campaignID:(NSString *)campaignID delegate:(id)delegate;
+(void)showInterstitial:(NSString *)mediaID identifier:(NSString*)identifier campaignType:(GreeAdsRewardCampaignType)type delegate:(id)delegate;

//click campaign with a campaignID
+ (void)clickCampaign:(NSString *)campaignID mediaID:(NSString*)mediaID identifier:(NSString*)identifier;

//tracking action for advertisement
+(void)sendActionWithCampaignID:(NSString *)campaignID advertisement:(NSString *)advertisement launchingOptions:(NSDictionary *)launchOptions;
+(void)sendActionWithCampaignID:(NSString *)campaignID advertisement:(NSString *)advertisement openURL:(NSURL *)openURL;
+(void)sendActionWithCampaignID:(NSString *)campaignID advertisement:(NSString *)advertisement callBackURL:(NSString *)calBackURL;
+(void)sendActionWithCampaignID:(NSString *)campaignID advertisement:(NSString *)advertisement;

@end
