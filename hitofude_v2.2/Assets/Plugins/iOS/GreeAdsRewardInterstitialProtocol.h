//
//  GreeAdsRewardInterstitialProtocol.h
//  GreeAdsReward
//
//  Copyright 2013 GREE, inc. All rights reserved.
//
//

#import <Foundation/Foundation.h>

@protocol GreeAdsRewardInterstitialProtocol <NSObject>

// Called before start requesting an interstitial from the back-end
-(void)onGreeAdsRewardInterstitialStartLoading;

// Called before interstitial appeared, you may want to pause your app here
-(void)onGreeAdsRewardInterstitialViewWillAppear;

// Called after interstitial appeared
-(void)onGreeAdsRewardInterstitialViewDidAppear;

// Called before interstitial Closed
-(void)onGreeAdsRewardInterstitialViewWillClose;

// Called after interstitial closed, you should resume your app here if you paused it before interstitial appeared
-(void)onGreeAdsRewardInterstitialViewDidClose;

// Called when interstitial failed to request from server, interstitial view will not be poped up
-(void)onGreeAdsRewardInterstitialLoadFailed;

@end
