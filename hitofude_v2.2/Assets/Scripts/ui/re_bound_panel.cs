﻿using UnityEngine;
using System.Collections;

public class re_bound_panel : MonoBehaviour {

	public UI_controller ui;

	// Use this for initialization
	void Start () {

		reclip ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void reclip(){
		float y_top = 320.0f;
		float y_bottom= ui.hitofude_data.settings.get_panel_level_offset_y_bottom();


		
		GetComponent<UIPanel> ().clipRange=new Vector4 (0,((y_top+y_bottom)/2.0f)*Screen.width / 640.0f, Screen.width ,(y_top-y_bottom)*Screen.width / 640.0f);

		//GetComponent<UIDraggablePanel> ().refresh_should_move ();

	}
}
