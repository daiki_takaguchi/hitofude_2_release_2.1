﻿using UnityEngine;
using System.Collections;

public class color_controller : MonoBehaviour {

	// Use this for initialization
	public UI_controller ui;

	public UIWidget uiwidget;


	public enum color_type{
		base_color,
		white,
		red,
		button_unavailable,
		button_unavailable_label
	}

	public color_type color;

	void Start () {
		set_color ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void set_color(){

		Color temp= ui.hitofude_data.settings.base_color;

		if (color == color_type.base_color) {
			temp= ui.hitofude_data.settings.base_color;
		}else if(color==color_type.white){
			temp= ui.hitofude_data.settings.white;
		}else if(color==color_type.red){
			temp= ui.hitofude_data.settings.red;
		}else if(color==color_type.button_unavailable){
			temp= ui.hitofude_data.settings.button_unavailable;
		}else if(color==color_type.button_unavailable_label){
			temp= ui.hitofude_data.settings.button_unavailable_label;
		}

		uiwidget.color=temp;
	}


}
